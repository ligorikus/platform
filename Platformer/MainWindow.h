#pragma once

#include "SDL.h"

class MainWindow
{
public:
	MainWindow();
	~MainWindow();
	void Run();
	bool IsNull();
	void GameLoop();
	void ProcessInput();
private:
	SDL_Window *mainWindow;
	const unsigned int maxFPS = 60;
	const struct WindowSize {
		int w = 500;	// ������
		int h = 100;	// ������
	} windowSize;	// ������ ����
	bool isRunning;

	void InitSystem();
};

