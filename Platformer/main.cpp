#include <iostream>
#include "MainWindow.h"

int main(int argc, char* args[])
{
	MainWindow gameWindow;
	gameWindow.Run();

	// ��������, ��� ���� ������� �������
	if (gameWindow.IsNull())
	{
		std::cout << "Could not create window:" << SDL_GetError() << std::endl;
		return 1;
	}
	
	return 0;
}