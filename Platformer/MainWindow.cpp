#include "MainWindow.h"
#include <iostream>


MainWindow::MainWindow()
{
	mainWindow = nullptr;
}

MainWindow::~MainWindow()
{
	// ��������� � ���������� ����
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
}

void MainWindow::Run()
{
	InitSystem();
	this->isRunning = true;
	GameLoop();
}

void MainWindow::InitSystem()
{
	// ������������� SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	// �������� ���� ����������
	mainWindow = SDL_CreateWindow(
		"MainWindow",	// ��������� ����
		SDL_WINDOWPOS_UNDEFINED,	// ������� �� x
		SDL_WINDOWPOS_UNDEFINED,	// ������� �� y
		this->windowSize.w,	// ������ ����
		this->windowSize.h,	// ������ ����
		SDL_WINDOW_OPENGL	// �����
	);
}

bool MainWindow::IsNull()
{
	if (this->mainWindow == nullptr) return true;
	else return false;
}

void MainWindow::GameLoop()
{
	while (this->isRunning)
	{
		ProcessInput();
	}
}

void MainWindow::ProcessInput()
{
	SDL_Event event;
	
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			this->isRunning = false;
			break;
		case SDL_MOUSEMOTION:
			std::cout << event.motion.x << ":" << event.motion.y << std::endl;
			break;
		}
	}
}
